﻿using System;
namespace GMO.LockApp.Interfaces.LocalAuthentications
{
    public class LocalAuthResult
    {
        public LocalAuthResult(LocalAuthStatus status, string errorMessage = null)
        {
            Status = status;
            ErrorMessage = errorMessage;
        }
        public LocalAuthStatus Status { get; private set; }
        public string ErrorMessage { get; private set; }
    }
}
