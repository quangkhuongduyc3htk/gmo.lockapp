﻿using System;
using System.Threading.Tasks;

namespace GMO.LockApp.Interfaces.LocalAuthentications
{
    /// <summary>
    /// interact with device fingerprint/faceId
    /// </summary>
    public interface ILocalAuthenticationService
    {
        /// <summary>
        /// indicate that the device has compabity hardware
        /// </summary>
        /// <returns><c>true</c>, if supported was ised, <c>false</c> otherwise.</returns>
        bool IsSupported();

        /// <summary>
        /// Authenticates the.
        /// </summary>
        /// <returns>The async.</returns>
        /// <param name="reason">Reason.</param>
        Task<LocalAuthResult> AuthenticateAsync(string reason);
    }
}
