﻿using System;
namespace GMO.LockApp.Interfaces.LocalAuthentications
{
    /// <summary>
    /// Fingerprint result.
    /// </summary>
    public enum LocalAuthStatus
    {
        /// <summary>
        /// The feature not supported by hardware
        /// </summary>
        NotSupported,

        /// <summary>
        /// The permission not granted.
        /// </summary>
        PermissionNotGranted,

        /// <summary>
        /// Fingerprint/touchid/faceid not configured.
        /// </summary>
        NotEnrolled,

        /// <summary>
        /// error occurred
        /// </summary>
        Error,

        /// <summary>
        /// Validate failed
        /// </summary>
        Failed,

        /// <summary>
        /// The succeed.
        /// </summary>
        Succeed,
    }
}
