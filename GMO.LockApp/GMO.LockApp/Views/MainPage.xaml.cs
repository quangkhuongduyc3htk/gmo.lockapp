﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GMO.LockApp.ViewModels;
using Xamarin.Forms;

namespace GMO.LockApp.Views
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            //entry1.ReturnCommand = new Command(() => entry2.Focus());
            //entry2.ReturnCommand = new Command(() => entry3.Focus());
            //entry3.ReturnCommand = new Command(() => entry4.Focus());
        }

        void Handle_TextChanged(object sender, Xamarin.Forms.TextChangedEventArgs e)
        {
            if (sender == entry1)
            {
                if (string.IsNullOrEmpty(entry1.Text))
                {

                }
                else
                {
                    entry2.Focus();
                }
            }
            else if (sender == entry2)
            {
                if (string.IsNullOrEmpty(entry2.Text))
                {
                    entry1.Focus();
                }
                else
                {
                    entry3.Focus();
                }
            }
            else if (sender == entry3)
            {
                if (string.IsNullOrEmpty(entry3.Text))
                {
                    entry2.Focus();
                }
                else
                {
                    entry4.Focus();
                }
            }
            else if (sender == entry4)
            {
                if (string.IsNullOrEmpty(entry4.Text))
                {
                    entry3.Focus();
                }
                else
                {
                    HandleLogin_Clicked(null, null);
                }
            }
        }
        private MainPageViewModel ViewModel => BindingContext as MainPageViewModel;

        void HandleLogin_Clicked(object sender, System.EventArgs e)
        {
            bool? status = ViewModel?.IsLoginSuccess();
            if (!(bool)status)
            {
                // Login fail
                Device.BeginInvokeOnMainThread(() =>
                {
                    ShakeAnimation(entry1);
                    ShakeAnimation(entry2);
                    ShakeAnimation(entry3);
                    ShakeAnimation(entry4);
                });
            }
        }

        async void ShakeAnimation(Entry entry)
        {
            uint timeout = 50;
            await entry.TranslateTo(-15, 0, timeout);
            await entry.TranslateTo(15, 0, timeout);
            await entry.TranslateTo(-10, 0, timeout);
            await entry.TranslateTo(10, 0, timeout);
            await entry.TranslateTo(-5, 0, timeout);
            await entry.TranslateTo(5, 0, timeout);
            entry.TranslationX = 0;
        }
    }
}