using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Prism.Commands;
using Prism.Navigation;
using Prism.Logging;
using Prism.Services;
using GMO.LockApp.Interfaces.LocalAuthentications;
using Acr.UserDialogs;

namespace GMO.LockApp.ViewModels
{
    public class HomePageViewModel : ViewModelBase
    {
        private readonly ILocalAuthenticationService _localAuthService;
        private bool isUnlock = false;
        public HomePageViewModel(INavigationService navigationService, ILocalAuthenticationService localAuthService) : base(navigationService)
        {
            _localAuthService = localAuthService;
            Title = "Home";
        }

        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            base.OnNavigatedTo(parameters);
            if (parameters != null && parameters.Count > 0)
            {
                isUnlock = parameters.GetValue<bool>("unlock");
            }
        }

        public override void OnResume()
        {
            base.OnResume();
            if (isUnlock)
            {
                isUnlock = false;
                return;
            }
            GoToUnLockPage();
        }

        private void GoToUnLockPage()
        {
            NavigationParameters keyValues = new NavigationParameters
            {
                {"lock", true}
            };
            NavigationService.NavigateAsync("MainPage", keyValues, useModalNavigation: true);
        }
    }
}
