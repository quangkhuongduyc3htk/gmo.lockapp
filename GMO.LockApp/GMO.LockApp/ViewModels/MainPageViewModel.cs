﻿using Acr.UserDialogs;
using GMO.LockApp.Interfaces.LocalAuthentications;
using Prism.Commands;
using Prism.Navigation;

namespace GMO.LockApp.ViewModels
{
    public class MainPageViewModel : ViewModelBase
    {
        private string _pin1;
        public string Pin1
        {
            get { return _pin1; }
            set { SetProperty(ref _pin1, value); }
        }

        private string _pin2;
        public string Pin2
        {
            get { return _pin2; }
            set { SetProperty(ref _pin2, value); }
        }

        private string _pin3;
        public string Pin3
        {
            get { return _pin3; }
            set { SetProperty(ref _pin3, value); }
        }

        private string _pin4;
        public string Pin4
        {
            get { return _pin4; }
            set { SetProperty(ref _pin4, value); }
        }

        private readonly ILocalAuthenticationService _localAuthService;
        private bool isLock = false;
        public MainPageViewModel(INavigationService navigationService, ILocalAuthenticationService localAuthService)
            : base(navigationService)
        {
            Title = "Lock App";
            _localAuthService = localAuthService;
        }

        //public DelegateCommand LoginCommand
        //{
        //    get { return new DelegateCommand(HandleAction); }
        //}

        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            base.OnNavigatedTo(parameters);
            if (parameters != null && parameters.Count > 0)
            {
                isLock = parameters.GetValue<bool>("lock");
            }
        }

        public bool IsLoginSuccess()
        {
            if ((Pin1 + Pin2 + Pin3 + Pin4)?.Length == 4)
            {
                GoToHomePage();
                return true;
            }
            return false;
        }

        public override void OnAppearing()
        {
            base.OnAppearing();
            UnLockScreen();
        }

        private async void UnLockScreen()
        {
            var isSupported = _localAuthService.IsSupported();
            if (!isSupported)
            {
                await UserDialogs.Instance.AlertAsync("Your device not support fingerprint authentication!", null, "OK");
                return;
            }
            var rs = await _localAuthService.AuthenticateAsync("Confirm unlock");
            switch (rs.Status)
            {
                case LocalAuthStatus.Succeed:
                    GoToHomePage(true);
                    break;
                default:
                    await UserDialogs.Instance.AlertAsync("Error", null, "OK");
                    break;
            }
        }

        private void GoToHomePage(bool useFaceID = false)
        {
            NavigationParameters keyValues = new NavigationParameters
            {
                {"unlock", useFaceID},
            };
            if (isLock)
            {
                NavigationService.GoBackAsync(keyValues);
                return;
            }
            NavigationService.NavigateAsync("../HomePage", keyValues);
        }
    }
}
