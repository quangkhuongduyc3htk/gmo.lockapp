﻿using System;
using System.Threading.Tasks;
using Foundation;
using GMO.LockApp.Interfaces.LocalAuthentications;
using LocalAuthentication;
using UIKit;
using Xamarin.Forms;

namespace GMO.LockApp.iOS.Services
{
    public class TouchIdService : ILocalAuthenticationService
    {
        public bool IsSupported()
        {
            using (var context = BuildContext(string.Empty))
            {
                var supported = IsSupported(context);
                return supported;
            }
        }

        public async Task<LocalAuthResult> AuthenticateAsync(string reason)
        {
            var context = BuildContext(reason);
            var rs = await AuthOnMainThreadAsync(context, reason);
            context.Dispose();
            return rs;
        }

        #region privates
        // temporay use
        private const string DeniedMessage = "User has denied the use of biometry for this app.";

        private LAContext BuildContext(string reason)
        {
            var context = new LAContext
            {
                LocalizedFallbackTitle = "Fallback" // iOS 8
            };
            if (UIDevice.CurrentDevice.CheckSystemVersion(10, 0))
            {
                context.LocalizedCancelTitle = "Cancel"; // iOS 10
            }
            if (UIDevice.CurrentDevice.CheckSystemVersion(11, 0))
            {
                context.LocalizedReason = reason; // iOS 11
            }
            return context;
        }

        bool IsPermissionGranted(LAContext context)
        {
            var result = context.CanEvaluatePolicy(LAPolicy.DeviceOwnerAuthenticationWithBiometrics, out var error);
            if (result)
                return true;
            var status = (LAStatus)(int)error.Code;
            result = status == LAStatus.BiometryNotAvailable && (error?.ToString() ?? string.Empty).Contains(DeniedMessage, StringComparison.OrdinalIgnoreCase);
            return !result;
        }

        /// <summary>
        /// Auths the on main thread.
        /// </summary>
        /// <returns>The on main thread.</returns>
        /// <param name="context">Context.</param>
        /// <param name="reason">Reason can not be null or empty</param>
        private Task<LocalAuthResult> AuthOnMainThreadAsync(LAContext context, string reason)
        {
            var tcs = new TaskCompletionSource<LocalAuthResult>();
            LocalAuthResult result = null;

            /* ==================================================================================================
             * indicate not allow null or empty reason
             * ================================================================================================*/
            if (string.IsNullOrWhiteSpace(reason))
            {
                result = new LocalAuthResult(LocalAuthStatus.Error, "Your reason can not be null or empty");
                tcs.SetResult(result);
                return tcs.Task;
            }
            if (!IsSupported(context))
                return Task.FromResult(new LocalAuthResult(LocalAuthStatus.NotSupported, "Your device not support TouchId/FaceId"));

            if (!IsPermissionGranted(context))
                return Task.FromResult(new LocalAuthResult(LocalAuthStatus.PermissionNotGranted, "Please allow to access your device's TouchId/FaceId"));

            if (!IsEnrolled(context))
                return Task.FromResult(new LocalAuthResult(LocalAuthStatus.NotEnrolled, "Your TouchId/FaceId look seem not configured"));

            /* ==================================================================================================
             * begin auth
             * ================================================================================================*/
            var nsReason = new NSString(reason);
            Device.BeginInvokeOnMainThread(() =>
            {
                var evaluateTask = context.EvaluatePolicyAsync(LAPolicy.DeviceOwnerAuthenticationWithBiometrics, nsReason);

                evaluateTask.ContinueWith(t =>
                {
                    var rs = t.Result;
                    result = new LocalAuthResult(rs.Item1 ? LocalAuthStatus.Succeed : LocalAuthStatus.Failed, rs.Item2?.ToString());
                    tcs.SetResult(result);
                });
            });

            return tcs.Task;
        }

        bool IsSupported(LAContext context)
        {
            var result = context.CanEvaluatePolicy(LAPolicy.DeviceOwnerAuthenticationWithBiometrics, out var error);
            if (result)
                return true;
            var status = (LAStatus)(int)error.Code;
            result = status != LAStatus.BiometryNotAvailable || (error?.ToString() ?? string.Empty).Contains(DeniedMessage, StringComparison.OrdinalIgnoreCase);
            return result;
        }

        bool IsEnrolled(LAContext context)
        {
            var result = context.CanEvaluatePolicy(LAPolicy.DeviceOwnerAuthenticationWithBiometrics, out var error);
            if (result)
                return true;
            var status = (LAStatus)(int)error.Code;

            result = status != LAStatus.BiometryNotEnrolled
                && status != LAStatus.BiometryNotAvailable;

            return result;
        }
        #endregion
    }
}
