﻿using System;
using Android.Hardware.Fingerprints;
using Android.OS;
using Android.Runtime;
using GMO.LockApp.Interfaces.LocalAuthentications;
using Java.Lang;

namespace GMO.LockApp.Droid.Services
{
    public class FingerprintHandler : FingerprintManager.AuthenticationCallback
    {
        Action<LocalAuthResult> _callback;
        public FingerprintHandler()
        {

        }
        protected FingerprintHandler(IntPtr javaReference, JniHandleOwnership transfer)
           : base(javaReference, transfer)
        {

        }
        public void Start(FingerprintManager manager, FingerprintManager.CryptoObject cryptoObject, CancellationSignal cancellationSignal, Action<LocalAuthResult> callback)
        {
            try
            {
                _callback = callback;
                manager.Authenticate(cryptoObject, cancellationSignal, 0, this, null);
            }
            catch (System.Exception ex)
            {
                //LogCommon.Error(ex);
                _callback?.Invoke(new LocalAuthResult(LocalAuthStatus.Error, ex.Message));
            }
        }

        public override void OnAuthenticationError([GeneratedEnum] FingerprintState errorCode, ICharSequence errString)
        {
            try
            {
                _callback?.Invoke(new LocalAuthResult(LocalAuthStatus.Error, errString?.ToString()));
            }
            catch (ObjectDisposedException ex)
            {
                //LogCommon.Error(ex);
            }
        }

        public override void OnAuthenticationFailed()
        {
            try
            {
                _callback?.Invoke(new LocalAuthResult(LocalAuthStatus.Failed, "Could not regconize you"));
            }
            catch (ObjectDisposedException ex)
            {
                //LogCommon.Error(ex);
            }
        }

        public override void OnAuthenticationHelp([GeneratedEnum] FingerprintState helpCode, ICharSequence helpString)
        {
            try
            {
                _callback?.Invoke(new LocalAuthResult(LocalAuthStatus.Failed, helpString?.ToString()));
            }
            catch (ObjectDisposedException ex)
            {
                //LogCommon.Error(ex);
            }
        }

        public override void OnAuthenticationSucceeded(FingerprintManager.AuthenticationResult result)
        {
            try
            {
                _callback?.Invoke(new LocalAuthResult(LocalAuthStatus.Succeed, null));
            }
            catch (ObjectDisposedException ex)
            {
                //LogCommon.Error(ex);
            }
        }
    }
}
