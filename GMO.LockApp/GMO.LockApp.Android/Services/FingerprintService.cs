﻿using System.Threading.Tasks;
using Android.Content;
using Android.Hardware.Fingerprints;
using System;
using Android.Support.V7.App;
using Plugin.CurrentActivity;
using Android;
using Java.Security;
using Javax.Crypto;
using Android.Security.Keystore;
using Android.OS;
using GMO.LockApp.Interfaces.LocalAuthentications;

namespace GMO.LockApp.Droid.Services
{
    public class FingerprintService : ILocalAuthenticationService
    {
        CancellationSignal _cancellationSignal;
        AlertDialog _dialog;
        KeyStore _keyStore;
        const string AndroidKeyStoreType = "AndroidKeyStore";
        const string KeyName = "androidHive";
        Cipher _cipher;

        public bool IsSupported()
        {
            if (!IsFromApi23())
                return false;

            var context = Android.App.Application.Context;

            /* ==================================================================================================
             * android api 23 or higher
             * ================================================================================================*/
            var manager = context.GetSystemService(Context.FingerprintService) as FingerprintManager;
            return manager.IsHardwareDetected;
        }

        public Task<LocalAuthResult> AuthenticateAsync(string reason)
        {
            if (!IsFromApi23())
                return Task.FromResult(new LocalAuthResult(LocalAuthStatus.NotSupported, "Only support from API level 23"));

            if (!IsSupported())
                return Task.FromResult(new LocalAuthResult(LocalAuthStatus.NotSupported, "Your device not support fingerprint"));

            if (!IsPermissionGranted())
                return Task.FromResult(new LocalAuthResult(LocalAuthStatus.PermissionNotGranted, "Please allow to access your device's fingerprint"));

            if (!IsEnrolled())
                return Task.FromResult(new LocalAuthResult(LocalAuthStatus.NotEnrolled, "Please setup at least one fingerprint on your device"));

            GenerateKey();
            if (!CipherInit())
                return Task.FromResult(new LocalAuthResult(LocalAuthStatus.Error, "Could not init the cipher"));

            /* ==================================================================================================
             * ensure previouse authentication be canceled
             * ================================================================================================*/
            Cleanup();

            /* ==================================================================================================
             * begin auth
             * ================================================================================================*/
            _dialog = BuildInfoDialog(reason);
            _dialog.Show();

            var tcs = new TaskCompletionSource<LocalAuthResult>();
            var manager = CrossCurrentActivity.Current.Activity.GetSystemService(Context.FingerprintService) as FingerprintManager;
            var cryptoObject = new FingerprintManager.CryptoObject(_cipher);
            var fingerprintHandler = new FingerprintHandler();
            _cancellationSignal = new CancellationSignal();
            fingerprintHandler.Start(manager, cryptoObject, _cancellationSignal, (rs) =>
            {
                Cleanup();
                tcs.SetResult(rs);
                fingerprintHandler.Dispose();
            });

            return tcs.Task;
        }

        #region privates
        bool IsEnrolled()
        {
            if (!IsFromApi23())
                return false;
            var context = Android.App.Application.Context;

            /* ==================================================================================================
             * android api 23 or higher
             * ================================================================================================*/
            var manager = context.GetSystemService(Context.FingerprintService) as FingerprintManager;
            return manager.HasEnrolledFingerprints;
        }

        private void GenerateKey()
        {
            try
            {
                if (!IsFromApi23())
                    return;

                _keyStore = KeyStore.GetInstance(AndroidKeyStoreType);
                KeyGenerator keyGenerator = KeyGenerator.GetInstance(KeyProperties.KeyAlgorithmAes, AndroidKeyStoreType);
                _keyStore.Load(null);
                keyGenerator.Init(new KeyGenParameterSpec.Builder(KeyName,
                                                                KeyStorePurpose.Encrypt | KeyStorePurpose.Decrypt)
                                                                .SetBlockModes(KeyProperties.BlockModeCbc)
                                                                .SetUserAuthenticationRequired(true)
                                                                .SetEncryptionPaddings(KeyProperties.EncryptionPaddingPkcs7)
                                                                .Build());
                keyGenerator.GenerateKey();
            }
            catch (Exception ex)
            {
                //LogCommon.Error(ex);
            }
        }

        private bool CipherInit()
        {
            try
            {
                if (!IsFromApi23())
                    return false;

                _cipher = Cipher.GetInstance($"{KeyProperties.KeyAlgorithmAes}/{KeyProperties.BlockModeCbc}/{KeyProperties.EncryptionPaddingPkcs7}");
                _keyStore.Load(null);
                var key = _keyStore.GetKey(KeyName, null);
                _cipher.Init(CipherMode.EncryptMode, key);
                return true;
            }
            catch (Exception ex)
            {
                //LogCommon.Error(ex);
                return false;
            }
        }

        void Cleanup()
        {
            try
            {
                _cancellationSignal?.Cancel();
                _cancellationSignal?.Dispose();
                _dialog?.Dismiss();
                _dialog?.Dispose();
            }
            catch (ObjectDisposedException ex)
            {
                // ignore this exception
                //LogCommon.Error(ex);
            }
        }

        bool IsPermissionGranted() => CrossCurrentActivity.Current.Activity.CheckSelfPermission(Manifest.Permission.UseFingerprint) == Android.Content.PM.Permission.Granted;

        bool IsFromApi23() => Build.VERSION.SdkInt >= BuildVersionCodes.M;

        AlertDialog BuildInfoDialog(string reason)
        {
            var builder = new AlertDialog.Builder(CrossCurrentActivity.Current.Activity);
            builder.SetTitle("Authentication");
            builder.SetMessage(reason);
            var localizedCancel = "Cancel";//TranslateExtension.GetLocalizedString("MessageButtonCancel");
            builder.SetNegativeButton(localizedCancel, (sender, e) =>
            {
                Cleanup();
            });
            builder.SetCancelable(false);
            builder.SetIcon(Android.Resource.Drawable.IcSecure);
            return builder.Create();
        }
        #endregion
    }
}
